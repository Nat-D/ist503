"""
Environment for IST503 Final Exam
written by Nat Dilokthanakul (natd_pro@vistec.ac.th) 
May 2020

This is the pipe environment.
Our worm-like robot walk on a complex pipe system.  
Its goal is to inspect the pipe system and fix a broken pipe section.

There are 3 scenarios
1. known environment with a fixed broken spot. 
2. known environment with stochastic reward function P(r|s) is known. 
3. unknown environment (i.e. The robot needs to discover the policy or the map.)
"""

"""
 Map
 2 = pipe
"""
import numpy as np
import cv2

MAP1 = [[2,2,2,2,2,2,0,0,0,0],
        [2,0,0,0,0,2,0,0,0,0],
        [2,2,2,0,0,2,0,0,0,0],
        [0,0,2,2,2,2,2,2,2,0],
        [0,0,2,0,0,2,0,0,2,0],
        [2,2,2,0,0,2,0,0,2,0],
        [2,0,2,0,0,2,0,0,2,0],
        [2,2,2,2,2,2,2,2,2,0],
        [0,0,2,0,0,0,0,0,2,0]]

MAP1 = np.array(MAP1)


class Pipe():
    def __init__(self, map, stochastic=False):

        self.step_count = 0

        self.agent = [0, 0] 
        self.goal =  [5, 0] # this is unknown to the agent (i.e. agent need to search)
        
        self.pipe = np.zeros(np.shape(map))  + (map == 2).astype(np.int32)
        self.pipe_padded = np.pad(self.pipe, [1,1], mode='constant', constant_values=0)

        self.terminate = False
        if stochastic:
            self.stochastic = True
        else:
            self.stochastic = False

    def reset(self):
        self.terminate = False
        self.agent = [0,0]
        self.goal = [5,0]
        self.step_count = 0

        return self.agent

    def heuristic(self, state):
        # a heuristic value of a state
        # Think of this as a radar sensor. 
        # The radar can detect fault pipe behaviour from a distance.
        return np.abs(state[0] - self.goal[0]) + np.abs(state[1] -self.goal[1])

    def expand_node(self, agent):
        """
        return possible transitions
        successor, action, step_cost, terminate

        """
        successors = []
        actions = []
        step_costs = []
        terminates = []

        agent_h = agent[0] + 1 # padded
        agent_w = agent[1] + 1 # padded

        if self.pipe_padded[agent_h-1, agent_w] == 1:
            successors += [[agent[0]-1, agent[1]]]
            actions += [0] 
            if self.check_goal([agent[0]-1, agent[1]]):
                step_costs += [100]
                terminates += [True]
            else:
                step_costs += [-1]
                terminates += [False]

        if self.pipe_padded[agent_h, agent_w-1] == 1:
            successors += [[agent[0], agent[1]-1]]
            actions += [1] 
            if self.check_goal([agent[0], agent[1]-1]):
                step_costs += [100]
                terminates += [True]
            else:
                step_costs += [-1]
                terminates += [False]

        if self.pipe_padded[agent_h+1, agent_w] == 1:
            successors += [[agent[0]+1, agent[1]]]
            actions += [2] 
            if self.check_goal([agent[0]+1, agent[1]]):
                step_costs += [100]
                terminates += [True]
            else:
                step_costs += [-1]
                terminates += [False]

        if self.pipe_padded[agent_h, agent_w+1] == 1:
            successors += [[agent[0], agent[1]+1]]
            actions += [3] 
            if self.check_goal([agent[0], agent[1]+1]):
                step_costs += [100]
                terminates += [True]
            else:
                step_costs += [-1]
                terminates += [False]

        if self.stochastic:
            if self.step_count > 500:
                terminates = np.ones_like(terminates)
            else:
                terminates = np.zeros_like(terminates)

        return successors, actions, step_costs, terminates

    def check_goal(self, agent):
        if agent == self.goal:
            return True
        else:
            return False

    def step(self, action):
        if self.stochastic:
            #self.goal = [5, 0] or [8,8] or [4,5]
            eps = np.random.rand()
            if eps < 0.2:
                self.goal = [5,0]
            elif eps < 0.4:
                self.goal = [8,8]
            else:
                self.goal = [4,5]

        children_nodes = self.expand_node(self.agent)
        action_cost = -1
        terminate = False

        for successor, act, cost, term in zip(*children_nodes):
            if action == act:
                self.agent = successor
                action_cost = cost
                terminate = term

        if terminate:
            self.terminate = True

        self.step_count += 1
        
        return self.agent, action_cost, self.terminate



def convert_map_to_rgb(map, agent, goal):

    pipe  = (map == 2).astype(np.int32)

    canvas_b = np.zeros(np.shape(map)) + pipe
    canvas_r = np.zeros(np.shape(map))
    canvas_g = np.zeros(np.shape(map))

    canvas_r[agent[0], agent[1]] = 1.0
    canvas_g[goal[0], goal[1]]  = 1.0

    canvas_b[agent[0], agent[1]] = 0
    canvas_b[goal[0], goal[1]]  = 0

    canvas = np.stack([canvas_b, canvas_g, canvas_r], axis=2) #CV use bgr 
    return canvas



if __name__ == "__main__":

    
    pipe = Pipe(MAP1, stochastic=False)

    for i in range(100000):
        # random agent 
        state, cost, term = pipe.step(np.random.randint(4))
        # draw visualisation
        canvas = convert_map_to_rgb(MAP1, pipe.agent, pipe.goal)
        cv2.imshow('show', canvas)
        cv2.waitKey(20)

        if term:
            break