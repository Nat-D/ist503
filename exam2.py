"""
Exam Q2: Navigation in a known stochastic environment

Implement (i) Policy iteration (5 points) and (ii) Value iteration (5 points) 

Stochastic Enviroment:
    Similar to deterministic environment but
    At every step, the goal moves to  [5,0] with probability of 0.2 
                                  or [8,8] with probability of 0.6
                                  or [4,5] with probability of 0.2
    Actions = 0, 1, 2, 3
    Transition -- Every action is valid and deterministic. Agent will stay at the same place if it cannot go towards specified direction.
    Reward -- Each step costs -1 points. However, if agent transition to a goal state it get 100 point.
    Termination -- the environment terminates when agent make a total of 500 steps
"""


from environment import Pipe, MAP1, convert_map_to_rgb
import numpy as np
import cv2

class PolicyIteration():
    """
    Your answer here.
    """
    pass


class ValueIteration():
    """
    Your answer here.
    """
    pass


"""
Example 
"""
class RandomAgent():
    def __init__(self):
        pass

    # this is the policy: a mapping from state to action
    def step(self, state): 
        return np.random.randint(4)

if __name__=="__main__":

    env = Pipe(MAP1, stochastic=True)

    agent = RandomAgent()

    for n in range(10):
        state = env.reset()
        episode_reward = 0
        terminate = False

        while not terminate:
            action = agent.step(state)
            state, cost, term = env.step(action)
            episode_reward += cost

            # draw visualisation
            canvas = convert_map_to_rgb(MAP1, env.agent, env.goal)
            cv2.imshow('show', canvas)
            cv2.waitKey(20)
            terminate = term

        print(episode_reward)


