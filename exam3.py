"""
Exam Q3: Navigation in an unknown stochastic environment

Implement (i) Monte-carlo policy iteration 
      or (ii) Q-Learning algorithm (10 points)

Note: You can use the Stochastic_Pipe map to develop your agent,
      but we will test it in a test map with different layout.
"""

from environment import Pipe, MAP1
env = Pipe(MAP1, stochastic=True)


class MCPolicyIteration():
    """
    Your answer here.
    """
    pass

class QLearning():
    """
    Your answer here.
    """
    pass




