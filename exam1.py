"""
Exam Q1: Search and Navigation in a known deterministic environment / unknown goal

Implement (i) DFS (10 points)  (ii) A*Search (10 points)
to create paths from the start to the goal for our robot.


Deterministic Pipe Map:
    States = agent (x,y) coordinate
    Actions = 0, 1, 2, 3 --> corresponding to the agent move up, left, down, right
    Termination = True if reach the goal state, else False
"""
from environment import Pipe, MAP1
from collections import deque
import numpy as np
import cv2


"""
Example: Bredth First Search
"""
class BFS():
    def __init__(self):
        self.queue = deque()
        self.visited = []
        self.parents = {}  

    def search_and_visualise(self, env):
        current_agent_state = env.agent
        self.start_state = str(current_agent_state)
        self.queue.append(current_agent_state)
        

        try:
            while self.queue:

                node = self.queue.popleft()
                childrens = env.expand_node(node)


                # draw visit node
                canvas = draw_path(MAP1, self.visited, color='g')
                cv2.imshow('visited', canvas)
                cv2.waitKey(100)

                for successor, act, cost, term in zip(*childrens):
                    if successor not in self.visited:
                        self.parents[str(successor)] = str(node)
                        if term:
                            goal_state = successor
                            self.visited.append(successor)
                            raise None
                        else:
                            self.visited.append(successor)
                            self.queue.append(successor)
        except:
            print('Found')

        # back-tracking
        path = [str(goal_state)]
        while path[-1] != self.start_state:
            path.append(self.parents[path[-1]])
        path.reverse()

        return path



class DFS():
    """
    Your answer here.
    """
    pass

class AStar():
    """
    Your answer here.
    """
    pass




def draw_path(map, path, color='r'):
    pipe  = (map == 2).astype(np.int32)
    canvas_b = np.zeros(np.shape(map)) + pipe
    canvas_r = np.zeros(np.shape(map))
    canvas_g = np.zeros(np.shape(map))

    for node in path:
        if isinstance(node, str):   
            node = eval(node)
        if color=='r':
            canvas_r[node[0], node[1]] = 1.0
        elif color=='g':
            canvas_g[node[0], node[1]] = 1.0

    canvas = np.stack([canvas_b, canvas_g, canvas_r], axis=2) #CV use bgr 
    return canvas


if __name__ == "__main__":

    env = Pipe(MAP1, stochastic=False)


    bfs = BFS()
    # compute path
    path = bfs.search_and_visualise(env)

    # visualise path 
    canvas = draw_path(MAP1, path)
    cv2.imshow('show', canvas)
    cv2.waitKey(0)

